import logo from './images/logo.svg';
import StoryBookLogo from './images/storybook_logo.png';
import './App.css';

const App = () => {

  return (
    <div className="App">
      <header className="App-header">
        <a href='./home'>
          <img src={logo} className="App-logo" alt="logo" />
          <h1>Project</h1>
        </a>
        <a href='./storybook'>
          <img src={StoryBookLogo} className="App-logo" alt="logo"/>
          <h1>Demo</h1>
        </a>
      </header>
    </div>
  );
}

export default App;
