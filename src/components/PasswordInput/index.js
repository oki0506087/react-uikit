import React, { useEffect, useState, useRef } from 'react'

let timer

const debounce = (cb) => {
  if (timer) clearTimeout(timer)
  timer = setTimeout(() => {
    cb()
  }, 1000)
}

const PasswordInput = (props) => {
  const [realText, setRealText] = useState(props.defaultVal)
  const ref = useRef()

  const pointGen = (mask, num) => {
    return Array.apply(null, Array(num)).map(function() { return mask }).join('')
  }

  const delayEffect = () => {
    if (ref?.current) {
      ref.current.value = pointGen(props.mask, ref.current.value.length)
    }
  }

  const onChange = e => {
    const preVal = realText
    // insert cursor location
    const index = e.target.selectionStart
    const nowVal = e.target.value
    // increase length of input's value
    const incre = nowVal.length - preVal.length
    // increase text
    if (incre > 0) {
      var newStr = nowVal.slice(index - incre, index)
      // realText = preVal.slice(0, index - incre) + newStr + preVal.slice(index - incre)
      setRealText(preVal.slice(0, index - incre) + newStr + preVal.slice(index - incre))
    // delete text
    } else if (incre < 0) {
      setRealText(preVal.slice(0, index) + preVal.slice(index - incre))
    }
    // render mask effect
    if (nowVal.length > 0) {
      e.target.value = pointGen(props.mask, nowVal.length - 1) + nowVal.charAt(nowVal.length - 1)
      debounce(delayEffect, e.target)
    }
    // reset insert cursor location
    e.target.setSelectionRange(index, index)
  }

  const onBlur = e => {
    console.log('??', realText)
    if (props.onBlur instanceof Function) props.onBlur(realText)
  }

  useEffect(() => {
    if (props.onChange instanceof Function) props.onChange(realText)
  }, [realText, props.onChange])

  return (
    <span>
      <input
        ref={ref}
        type={props.type}
        onChange={onChange}
        onBlur={onBlur}
        pattern='0-9'
      />
      <input type='hidden' name={props.name} value={realText} />
    </span>
  )
}

PasswordInput.defaultProps = {
  defaultVal: '',
  mask: '*',
  name: 'test',
  type: 'tel',
  maxLength: null,
  onChange: () => {}
}

export default PasswordInput
